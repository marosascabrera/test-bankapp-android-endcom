package com.endcom.bankapp.form;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import io.reactivex.rxjava3.core.Single;

public interface FormMVP {
    interface View {
        void setToolbar();
        void bindUI();
        void buttonActions();
        void showAlertJson(JSONObject object);
        void showError(String message);

        String getNumber();
        String getAccount();
        String getIssure();
        String getName();
        String getBrand();
        String getStatus();
        String getBalance();
        String getTypeAccount();

    }

    interface Presenter {
        void setView(FormMVP.View view);
        void generateJson();
    }
}
