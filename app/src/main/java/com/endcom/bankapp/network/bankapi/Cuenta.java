
package com.endcom.bankapp.network.bankapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cuenta {

    @SerializedName("cuenta")
    @Expose
    private Long cuenta;
    @SerializedName("nombre")
    @Expose
    private String nombre;
    @SerializedName("ultimaSesion")
    @Expose
    private String ultimaSesion;
    @SerializedName("id")
    @Expose
    private Integer id;

    public Long getCuenta() {
        return cuenta;
    }

    public void setCuenta(Long cuenta) {
        this.cuenta = cuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUltimaSesion() {
        return ultimaSesion;
    }

    public void setUltimaSesion(String ultimaSesion) {
        this.ultimaSesion = ultimaSesion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
