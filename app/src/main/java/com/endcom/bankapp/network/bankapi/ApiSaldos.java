
package com.endcom.bankapp.network.bankapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiSaldos {

    @SerializedName("saldos")
    @Expose
    private List<Saldo> saldos = null;

    public List<Saldo> getSaldos() {
        return saldos;
    }

    public void setSaldos(List<Saldo> saldos) {
        this.saldos = saldos;
    }

}
