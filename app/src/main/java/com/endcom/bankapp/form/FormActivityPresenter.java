package com.endcom.bankapp.form;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FormActivityPresenter implements FormMVP.Presenter {

    private static final String TAG = FormActivityPresenter.class.getName();

    private FormMVP.View view;

    @Override
    public void setView(FormMVP.View view) {
        this.view = view;
    }

    @Override
    public void generateJson() {
        if (view == null) return;

        JSONObject json = new JSONObject();
        try {
            JSONObject object = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            object.put("number", view.getNumber());
            object.put("account", view.getAccount());
            object.put("issure", view.getIssure());
            object.put("name", view.getName());
            object.put("brand", view.getBrand());
            object.put("status", view.getStatus());
            object.put("balance", view.getBalance());
            object.put("type_account", view.getTypeAccount());
            jsonArray.put(object);
            json.put("registration", jsonArray);
        } catch (JSONException e) {
            view.showError("Falló al intentar crear el json.");
        } finally {
            view.showAlertJson(json);
        }
    }
}
