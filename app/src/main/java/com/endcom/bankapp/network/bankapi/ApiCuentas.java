
package com.endcom.bankapp.network.bankapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiCuentas {

    @SerializedName("cuenta")
    @Expose
    private List<Cuenta> cuenta = null;

    public List<Cuenta> getCuenta() {
        return cuenta;
    }

    public void setCuenta(List<Cuenta> cuenta) {
        this.cuenta = cuenta;
    }

}
