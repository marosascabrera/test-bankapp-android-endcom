package com.endcom.bankapp.main;

import com.endcom.bankapp.network.bankapi.Cuenta;
import com.endcom.bankapp.network.bankapi.Movimiento;
import com.endcom.bankapp.network.bankapi.Saldo;
import com.endcom.bankapp.network.bankapi.Tarjeta;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;

public interface MainActivityRepository {
    Observable<Cuenta> getAccountFromNetwork();
    Observable<Cuenta> getAccountFromCache();
    Observable<Cuenta> getAccountData();

    Observable<Saldo> getBalanceFromNetwork();
    Observable<Saldo> getBalanceFromCache();
    Observable<Saldo> getBalanceData();

    Observable<Tarjeta> getCardFromNetwork();
    Observable<Tarjeta> getCardFromCache();
    Observable<Tarjeta> getCardData();

    Observable<Movimiento> getTransactionFromNetwork();
    Observable<Movimiento> getTransactionFromCache();
    Observable<Movimiento> getTransactionData();
}
