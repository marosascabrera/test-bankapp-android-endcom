package com.endcom.bankapp.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.endcom.bankapp.R;
import com.endcom.bankapp.network.bankapi.Saldo;

import java.util.ArrayList;
import java.util.List;

public class MainActivityAdapterBalance extends RecyclerView.Adapter<MainActivityAdapterBalance.MainActivityViewHolder> {

    private final Context context;
    private final List<Saldo> lsSaldo;
    private final String[] titles;

    public MainActivityAdapterBalance(Context context) {
        this.context = context;
        this.lsSaldo = new ArrayList<>();
        titles = new String[]{"Saldo general en cuentas", "Total de ingresos", "Total de gastos"};
    }

    @NonNull
    @Override
    public MainActivityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_balance, parent, false);
        return new MainActivityViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainActivityViewHolder holder, int position) {
        holder.bind(lsSaldo, titles);
    }

    @Override
    public int getItemCount() {
        return titles.length;
    }

    public void addList(Saldo saldo) {
        lsSaldo.add(saldo);
        notifyItemInserted(lsSaldo.size() - 1);
    }

    @SuppressLint("NotifyDataSetChanged")
    public void clearList(){
        lsSaldo.clear();
        notifyDataSetChanged();
    }

    public static class MainActivityViewHolder extends RecyclerView.ViewHolder {

        private final AppCompatTextView tvBalanceTitle;
        private final AppCompatTextView tvBalanceQuantity;

        public MainActivityViewHolder(@NonNull View itemView) {
            super(itemView);
            tvBalanceTitle = itemView.findViewById(R.id.tv_title);
            tvBalanceQuantity = itemView.findViewById(R.id.tv_quantity);
        }

        public void bind(List<Saldo> lsSaldo, String[] titles) {
            Saldo saldo = lsSaldo.get(0);
            String title = titles[getAbsoluteAdapterPosition()];
            tvBalanceTitle.setText(title);
            int balance = 0;
            switch (getAbsoluteAdapterPosition()){
                case 0: balance = saldo.getSaldoGeneral(); break;
                case 1: balance = saldo.getIngresos(); break;
                case 2: balance = saldo.getGastos(); break;
            }
            tvBalanceQuantity.setText(String.format("$%s", balance));
        }
    }
}
