
package com.endcom.bankapp.network.bankapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiMovimientos {

    @SerializedName("movimientos")
    @Expose
    private List<Movimiento> movimientos = null;

    public List<Movimiento> getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(List<Movimiento> movimientos) {
        this.movimientos = movimientos;
    }

}
