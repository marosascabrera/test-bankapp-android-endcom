package com.endcom.bankapp.main;

import com.endcom.bankapp.network.bankapi.Cuenta;
import com.endcom.bankapp.network.bankapi.Movimiento;
import com.endcom.bankapp.network.bankapi.Saldo;
import com.endcom.bankapp.network.bankapi.Tarjeta;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;


public interface MainActivityMVP {
    interface View {
        void setToolbar();
        void bindUI();
        void actionTextView();
        void addCard();
        void configRecyclerBalance();
        void configRecyclerCards();
        void configRecyclerTransaction();

        void showAccount(Cuenta cuenta);
        void showBalances(Saldo saldo);
        void showCards(Tarjeta tarjeta);
        void showTransactions(Movimiento movimiento);

        void showSuccess(String message);
        void showError(String message);
    }

    interface Presenter {
        void setView(MainActivityMVP.View view);
        void rxJavaUnsubscribe();
        void loadAccount();
        void loadBalance();
        void loadCard();
        void loadTransaction();
    }

    interface Model {
         Observable<Cuenta> resultAccount();
         Observable<Saldo> resultBalance();
         Observable<Tarjeta> resultCard();
         Observable<Movimiento> resultTransaction();
    }
}
