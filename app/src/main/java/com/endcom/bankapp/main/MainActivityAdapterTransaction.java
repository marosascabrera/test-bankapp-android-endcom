package com.endcom.bankapp.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.endcom.bankapp.R;
import com.endcom.bankapp.network.bankapi.Movimiento;

import java.util.ArrayList;
import java.util.List;

public class MainActivityAdapterTransaction extends RecyclerView.Adapter<MainActivityAdapterTransaction.MainActivityTransactionViewHolder> {

    private final Context context;
    List<Movimiento> lsTransaction;

    public MainActivityAdapterTransaction(Context context) {
        this.context = context;
        this.lsTransaction = new ArrayList<>();
    }

    @NonNull
    @Override
    public MainActivityTransactionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_recent, parent, false);
        return new MainActivityTransactionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainActivityTransactionViewHolder holder, int position) {
        holder.bind(lsTransaction, context);
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public void addList(Movimiento movimiento) {
        lsTransaction.add(movimiento);
        notifyItemInserted(lsTransaction.size() - 1);
    }

    @SuppressLint("NotifyDataSetChanged")
    public void clearList() {
        lsTransaction.clear();
        notifyDataSetChanged();
    }

    public static class MainActivityTransactionViewHolder extends RecyclerView.ViewHolder {


        private final AppCompatTextView tvRecentTitle;
        private final AppCompatTextView tvRecentDate;
        private final AppCompatTextView tvRecentTotal;

        public MainActivityTransactionViewHolder(@NonNull View itemView) {
            super(itemView);
            tvRecentTitle = itemView.findViewById(R.id.tv_recent_title);
            tvRecentDate = itemView.findViewById(R.id.tv_recent_date);
            tvRecentTotal = itemView.findViewById(R.id.tv_recent_total);
        }

        public void bind(List<Movimiento> lsTransaction, Context context) {
            Movimiento movimiento = lsTransaction.get(getAbsoluteAdapterPosition());
            tvRecentTitle.setText("UBER BV");
            tvRecentDate.setText(movimiento.getFecha());
            int color = (movimiento.getTipo().equalsIgnoreCase("cargo")) ? R.color.color_primary : R.color.color_red;
            tvRecentTotal.setTextColor(context.getColor(color));
            tvRecentTotal.setText(String.format("$%s", movimiento.getMonto()));
        }
    }
}
