package com.endcom.bankapp.form;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;

import com.endcom.bankapp.R;
import com.endcom.bankapp.app.App;
import com.endcom.bankapp.main.MainActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import javax.inject.Inject;

public class FormActivity extends AppCompatActivity implements FormMVP.View {

    private static final String TAG = FormActivity.class.getName();

    private AppCompatEditText edtCardNumber;
    private AppCompatEditText edtCardAccount;
    private AppCompatEditText edtCardIssure;
    private AppCompatEditText edtCardName;
    private AppCompatEditText edtCardBrand;
    private AppCompatEditText edtCardStatus;
    private AppCompatEditText edtCardBalance;
    private AppCompatEditText edtCardTypeAccount;

    private AppCompatButton btnCancel;
    private AppCompatButton btnAccept;

    @Inject
    FormMVP.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        ((App) getApplication()).component.inject(this);

        setToolbar();
        bindUI();
        buttonActions();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
    }

    @Override
    public void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() == null) return;
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void bindUI() {
        edtCardNumber = findViewById(R.id.edt_card_number);
        edtCardAccount = findViewById(R.id.edt_card_account);
        edtCardIssure = findViewById(R.id.edt_card_issure);
        edtCardName = findViewById(R.id.edt_card_name);
        edtCardBrand = findViewById(R.id.edt_card_brand);
        edtCardStatus = findViewById(R.id.edt_card_status);
        edtCardBalance = findViewById(R.id.edt_card_balance);
        edtCardTypeAccount = findViewById(R.id.edt_card_account_type);

        btnCancel = findViewById(R.id.btn_cancel);
        btnAccept = findViewById(R.id.btn_accept);
    }

    @Override
    public void buttonActions() {
        btnCancel.setOnClickListener(view -> onBackPressed());
        btnAccept.setOnClickListener(view -> presenter.generateJson());
    }

    @Override
    public void showAlertJson(JSONObject object) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_dialog, viewGroup, false);

        AppCompatTextView tvJson = view.findViewById(R.id.tv_json);

        try {
            tvJson.setText(object.toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        builder.setView(view);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getNumber() {
        return Objects.requireNonNull(edtCardNumber.getText()).toString();
    }

    @Override
    public String getAccount() {
        return Objects.requireNonNull(edtCardAccount.getText()).toString();
    }

    @Override
    public String getIssure() {
        return Objects.requireNonNull(edtCardIssure.getText()).toString();
    }

    @Override
    public String getName() {
        return Objects.requireNonNull(edtCardName.getText()).toString();
    }

    @Override
    public String getBrand() {
        return Objects.requireNonNull(edtCardBrand.getText()).toString();
    }

    @Override
    public String getStatus() {
        return Objects.requireNonNull(edtCardStatus.getText()).toString();
    }

    @Override
    public String getBalance() {
        return Objects.requireNonNull(edtCardBalance.getText()).toString();
    }

    @Override
    public String getTypeAccount() {
        return Objects.requireNonNull(edtCardTypeAccount.getText()).toString();
    }
}
