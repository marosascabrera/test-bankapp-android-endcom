package com.endcom.bankapp.app;

import com.endcom.bankapp.form.FormActivity;
import com.endcom.bankapp.form.FormActivityModule;
import com.endcom.bankapp.main.MainActivity;
import com.endcom.bankapp.main.MainActivityModule;
import com.endcom.bankapp.network.BankApiModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        BankApiModule.class,
        MainActivityModule.class,
        FormActivityModule.class
})
public interface ApplicationComponent {
    void inject(MainActivity target);
    void inject(FormActivity target);
}
