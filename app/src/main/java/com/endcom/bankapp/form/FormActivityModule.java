package com.endcom.bankapp.form;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class FormActivityModule {
    @Singleton
    @Provides
    public FormMVP.Presenter provideFormPresenter() {
        return new FormActivityPresenter();
    }
}
