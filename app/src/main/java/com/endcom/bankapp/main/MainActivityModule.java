package com.endcom.bankapp.main;

import com.endcom.bankapp.network.BankApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {
    @Provides
    public MainActivityMVP.Presenter provideMainActivityPresenter(MainActivityMVP.Model model) {
        return new MainActivityPresenter(model);
    }

    @Provides
    public MainActivityMVP.Model provideMainActivityModel(MainActivityRepository repository) {
        return new MainActivityModel(repository);
    }

    @Provides
    @Singleton
    public MainActivityRepository provideMainActivityRepository(BankApiService service) {
        return new MainActivityRepositoryImpl(service);
    }
}
