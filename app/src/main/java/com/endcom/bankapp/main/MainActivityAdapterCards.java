package com.endcom.bankapp.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.endcom.bankapp.R;
import com.endcom.bankapp.network.bankapi.Tarjeta;

import java.util.ArrayList;
import java.util.List;

public class MainActivityAdapterCards extends RecyclerView.Adapter<MainActivityAdapterCards.MainActivityCardViewHolder> {

    private final Context context;
    private final List<Tarjeta> lsCards;

    public MainActivityAdapterCards(Context context) {
        this.context = context;
        this.lsCards = new ArrayList<>();
    }

    @NonNull
    @Override
    public MainActivityCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_status, parent, false);
        return new MainActivityCardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MainActivityCardViewHolder holder, int position) {
        holder.bind(lsCards);
    }

    @Override
    public int getItemCount() {
        return lsCards.size();
    }

    public void addList(Tarjeta tarjeta) {
        lsCards.add(tarjeta);
        notifyItemInserted(lsCards.size() - 1);
    }

    @SuppressLint("NotifyDataSetChanged")
    public void clearList(){
        lsCards.clear();
        notifyDataSetChanged();
    }

    public static class MainActivityCardViewHolder extends RecyclerView.ViewHolder {

        AppCompatImageView ivStatus;
        AppCompatTextView tvStatus;
        AppCompatTextView tvAmount;
        AppCompatTextView tvNumber;
        AppCompatTextView tvClient;
        AppCompatTextView tvOwner;

        public MainActivityCardViewHolder(@NonNull View itemView) {
            super(itemView);
            ivStatus = itemView.findViewById(R.id.image_status);
            tvStatus = itemView.findViewById(R.id.tv_status);
            tvAmount = itemView.findViewById(R.id.tv_amount);
            tvNumber = itemView.findViewById(R.id.tv_number);
            tvClient = itemView.findViewById(R.id.tv_name_client);
            tvOwner = itemView.findViewById(R.id.tv_owner);
        }

        public void bind(List<Tarjeta> lsTarjetas) {
            Tarjeta tarjeta = lsTarjetas.get(getAbsoluteAdapterPosition());
            int image = (tarjeta.getEstado().equals("activa")) ? R.drawable.ic_status_up : R.drawable.ic_status_down;
            ivStatus.setImageResource(image);
            tvStatus.setText(tarjeta.getEstado());
            tvAmount.setText(String.format("$%s", tarjeta.getSaldo()));
            tvNumber.setText(String.valueOf(tarjeta.getTarjeta()));
            tvClient.setText(tarjeta.getNombre());
            tvOwner.setText(tarjeta.getTipo());
        }
    }
}
