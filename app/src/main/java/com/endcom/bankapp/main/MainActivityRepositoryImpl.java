package com.endcom.bankapp.main;

import com.endcom.bankapp.network.BankApiService;
import com.endcom.bankapp.network.bankapi.Cuenta;
import com.endcom.bankapp.network.bankapi.Movimiento;
import com.endcom.bankapp.network.bankapi.Saldo;
import com.endcom.bankapp.network.bankapi.Tarjeta;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.core.Observable;

public class MainActivityRepositoryImpl implements MainActivityRepository {

    private static final long CACHE_LIFETIME = 20 * 1000;

    private static final String TAG = MainActivityRepositoryImpl.class.getName();
    private final BankApiService service;
    private List<Cuenta> lsAccount;
    private List<Saldo> lsBalance;
    private List<Tarjeta> lsCard;
    private List<Movimiento> lsTransaction;

    private long lastTimestamp;


    public MainActivityRepositoryImpl(BankApiService service) {
        this.service = service;
        this.lsAccount = new ArrayList<>();
        this.lsBalance = new ArrayList<>();
        this.lsCard = new ArrayList<>();
        this.lsTransaction = new ArrayList<>();
    }

    private boolean isUpdate() {
        return (System.currentTimeMillis() - lastTimestamp) >= CACHE_LIFETIME;
    }

    @Override
    public Observable<Cuenta> getAccountFromNetwork() {
        lsAccount = service.getAccount().blockingFirst().getCuenta();
        return Observable.fromIterable(lsAccount);
    }

    @Override
    public Observable<Cuenta> getAccountFromCache() {
        if (isUpdate()) return Observable.fromIterable(lsAccount);

        lastTimestamp = System.currentTimeMillis();
        lsAccount.clear();
        return Observable.empty();
    }

    @Override
    public Observable<Cuenta> getAccountData() {
        return getAccountFromCache().switchIfEmpty(getAccountFromNetwork());
    }

    @Override
    public Observable<Saldo> getBalanceFromNetwork() {
        lsBalance = service.getBalance().blockingFirst().getSaldos();
        return Observable.fromIterable(lsBalance);
    }

    @Override
    public Observable<Saldo> getBalanceFromCache() {
        if (isUpdate()) return Observable.fromIterable(lsBalance);

        lastTimestamp = System.currentTimeMillis();
        lsAccount.clear();
        return Observable.empty();
    }

    @Override
    public Observable<Saldo> getBalanceData() {
        return getBalanceFromCache().switchIfEmpty(getBalanceFromNetwork());
    }

    @Override
    public Observable<Tarjeta> getCardFromNetwork() {
        lsCard = service.getCards().blockingIterable().iterator().next().getTarjetas();
        return Observable.fromIterable(lsCard);
    }

    @Override
    public Observable<Tarjeta> getCardFromCache() {
        if (isUpdate()) return Observable.fromIterable(lsCard);

        lastTimestamp = System.currentTimeMillis();
        lsAccount.clear();
        return Observable.empty();
    }

    @Override
    public Observable<Tarjeta> getCardData() {
        return getCardFromCache().switchIfEmpty(getCardFromNetwork());
    }

    @Override
    public Observable<Movimiento> getTransactionFromNetwork() {
        lsTransaction = service.getTransactions().blockingIterable().iterator().next().getMovimientos();
        return Observable.fromIterable(lsTransaction);
    }

    @Override
    public Observable<Movimiento> getTransactionFromCache() {
        if (isUpdate()) return Observable.fromIterable(lsTransaction);

        lastTimestamp = System.currentTimeMillis();
        lsTransaction.clear();
        return Observable.empty();
    }

    @Override
    public Observable<Movimiento> getTransactionData() {
        return getTransactionFromCache().switchIfEmpty(getTransactionFromNetwork());
    }
}
