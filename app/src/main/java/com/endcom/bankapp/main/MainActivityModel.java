package com.endcom.bankapp.main;

import android.util.Log;

import com.endcom.bankapp.network.bankapi.Cuenta;
import com.endcom.bankapp.network.bankapi.Movimiento;
import com.endcom.bankapp.network.bankapi.Saldo;
import com.endcom.bankapp.network.bankapi.Tarjeta;

import java.util.List;

import io.reactivex.rxjava3.core.Observable;

public class MainActivityModel implements MainActivityMVP.Model {

    private static final String TAG = MainActivityModel.class.getName();

    private final MainActivityRepository repository;

    public MainActivityModel(MainActivityRepository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<Cuenta> resultAccount() {
        return repository.getAccountData();
    }

    @Override
    public Observable<Saldo> resultBalance() {
        return repository.getBalanceData();
    }

    @Override
    public Observable<Tarjeta> resultCard() {
        return repository.getCardData();
    }

    @Override
    public Observable<Movimiento> resultTransaction() {
        return repository.getTransactionData();
    }
}
