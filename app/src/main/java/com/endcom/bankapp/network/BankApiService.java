package com.endcom.bankapp.network;


import com.endcom.bankapp.network.bankapi.ApiCuentas;
import com.endcom.bankapp.network.bankapi.ApiMovimientos;
import com.endcom.bankapp.network.bankapi.ApiSaldos;
import com.endcom.bankapp.network.bankapi.ApiTarjetas;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.GET;

public interface BankApiService {
    @GET("cuenta")
    Observable<ApiCuentas> getAccount();

    @GET("saldos")
    Observable<ApiSaldos> getBalance();

    @GET("tarjetas")
    Observable<ApiTarjetas> getCards();

    @GET("movimientos")
    Observable<ApiMovimientos> getTransactions();
}
