# BankApp
## Test de desarrollo móvil Android 

Este test de desarrollo solicita crear una aplicacion desde cero con base a los conocimientos que se tiene sobre desarrollo en android. 

### MVP (Model View Presenter)
- El MVP (Model View Presenter) es una derivación del patrón de diseño MVC (Model View Controller), el cual nos permite separar la parte lógica de las vistas de las aplicaciones.

### RxJava
- RxJava es una implementación open-source de la librería ReactiveX que le ayuda a crear aplicaciones en el estilo de programación reactivo. Aunque RxJava está diseñado para procesar flujos sincrónicas y asincrónicas de datos, no se restringe a tipos de datos "tradicional".

### Retrofit
- Retrofit es un cliente de servidores REST para Android y Java desarrollado por Square, muy simple y muy fácil de aprender. Permite hacer peticiones al servidor tipo: GET, POST, PUT, PATCH, DELETE y HEAD, y gestionar diferentes tipos de parámetros, paseando automáticamente la respuesta a un tipo de datos.

### Json
- JSON, cuyo nombre corresponde a las siglas JavaScript Object Notation o Notación de Objetos de JavaScript, es un formato ligero de intercambio de datos, que resulta sencillo de leer y escribir para los programadores y simple de interpretar y generar para las máquinas.

### Dagger
- Dagger genera automáticamente un código que imita el código que habrías escrito a mano. Debido a que el código se genera en tiempo de compilación, se puede rastrear y tiene más rendimiento que otras soluciones basadas en reflejos, como Guice.


## Como ejecutar la App
REQUISITOS MÍNIMOS
Android
Para que la app pueda funcionar correctamente en un dispositivo es necesario que cumpla con las 
siguientes condiciones.
1. Tener sistema operativo Android
1.1.Ser Smartphone
1.2.Funcionar con sistema operativo Android 6.0 o superior.
1.3.Tener espacio de almacenamiento disponible mayor o igual a 12 MB.

PASOS
1. Ejecutamos BankApp

![Iniciar app](https://i.ibb.co/56q56Gk/Screenshot-20210814-132943.png)

2. Mostrara el dashboar de la aplicación.

![Pantalla principal](https://i.ibb.co/xfG9jgf/Screenshot-20210814-132805.png)

3. Pantalla para registrar nueva tarjeta.

![Registro](ttps://i.ibb.co/xqJ75c3/Screenshot-20210814-132815.png)

4. Muestra los datos en formato json en caso de que posteriormente se necesiten enviar a una base de datos.

![Json](https://i.ibb.co/Y2bxjMd/Screenshot-20210814-132847.png)
