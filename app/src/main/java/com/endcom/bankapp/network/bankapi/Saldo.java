
package com.endcom.bankapp.network.bankapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Saldo {

    @SerializedName("cuenta")
    @Expose
    private Long cuenta;
    @SerializedName("saldoGeneral")
    @Expose
    private Integer saldoGeneral;
    @SerializedName("ingresos")
    @Expose
    private Integer ingresos;
    @SerializedName("gastos")
    @Expose
    private Integer gastos;
    @SerializedName("id")
    @Expose
    private Integer id;

    public Long getCuenta() {
        return cuenta;
    }

    public void setCuenta(Long cuenta) {
        this.cuenta = cuenta;
    }

    public Integer getSaldoGeneral() {
        return saldoGeneral;
    }

    public void setSaldoGeneral(Integer saldoGeneral) {
        this.saldoGeneral = saldoGeneral;
    }

    public Integer getIngresos() {
        return ingresos;
    }

    public void setIngresos(Integer ingresos) {
        this.ingresos = ingresos;
    }

    public Integer getGastos() {
        return gastos;
    }

    public void setGastos(Integer gastos) {
        this.gastos = gastos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
