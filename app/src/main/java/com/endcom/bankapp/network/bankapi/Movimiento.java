
package com.endcom.bankapp.network.bankapi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Movimiento {

    @SerializedName("fecha")
    @Expose
    private String fecha;
    @SerializedName("descripcion")
    @Expose
    private String descripcion;
    @SerializedName("monto")
    @Expose
    private String monto;
    @SerializedName("tipo")
    @Expose
    private String tipo;
    @SerializedName("id")
    @Expose
    private Integer id;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
