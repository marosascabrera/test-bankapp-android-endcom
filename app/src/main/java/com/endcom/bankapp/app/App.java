package com.endcom.bankapp.app;

import android.app.Application;

import com.endcom.bankapp.form.FormActivityModule;
import com.endcom.bankapp.main.MainActivityModule;
import com.endcom.bankapp.network.BankApiModule;

public class App extends Application {

    public ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .bankApiModule(new BankApiModule())
                .mainActivityModule(new MainActivityModule())
                .formActivityModule(new FormActivityModule())
                .build();
    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
