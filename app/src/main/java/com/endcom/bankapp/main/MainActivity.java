package com.endcom.bankapp.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.endcom.bankapp.R;
import com.endcom.bankapp.app.App;
import com.endcom.bankapp.form.FormActivity;
import com.endcom.bankapp.network.bankapi.Cuenta;
import com.endcom.bankapp.network.bankapi.Movimiento;
import com.endcom.bankapp.network.bankapi.Saldo;
import com.endcom.bankapp.network.bankapi.Tarjeta;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MainActivityMVP.View {

    private static final String TAG = MainActivity.class.getName();

    private AppCompatTextView tvAccountName;
    private AppCompatTextView tvAccountDate;
    private AppCompatTextView tvAddCard;
    private AppCompatTextView tvAccounts;
    private AppCompatTextView tvSendMoney;

    private RecyclerView rvBalance;
    private RecyclerView rvCard;
    private RecyclerView rvTransaction;
    private MainActivityAdapterBalance adapterBalance;
    private MainActivityAdapterCards adapterCard;
    private MainActivityAdapterTransaction adapterTransaction;

    @Inject
    MainActivityMVP.Presenter presenter;

    private boolean update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ((App) getApplication()).component.inject(this);

        setToolbar();
        bindUI();
        actionTextView();
        addCard();
        configRecyclerBalance();
        configRecyclerCards();
        configRecyclerTransaction();
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvAddCard.setTextColor(getColor(R.color.color_link));
        if (!update) {
            presenter.setView(this);
            presenter.loadAccount();
            presenter.loadBalance();
            presenter.loadCard();
            presenter.loadTransaction();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.rxJavaUnsubscribe();
        adapterBalance.clearList();
        adapterCard.clearList();
        adapterTransaction.clearList();
    }

    @Override
    public void setToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void bindUI() {
        tvAccountName = findViewById(R.id.tv_name);
        tvAccountDate = findViewById(R.id.tv_last_session);
        tvAccounts = findViewById(R.id.tv_accounts);
        tvSendMoney = findViewById(R.id.tv_send_money);
        tvAddCard = findViewById(R.id.tv_add_card);
        rvBalance = findViewById(R.id.rv_details);
        rvCard = findViewById(R.id.rv_status);
        rvTransaction = findViewById(R.id.rv_recent_moves);
    }

    @Override
    public void actionTextView() {
        tvAccounts.setOnClickListener(view -> {
            tvAccounts.setTextColor((tvAccounts.getCurrentTextColor() == getColor(R.color.color_link)) ? getColor(R.color.color_gray_dark) : getColor(R.color.color_link));
            tvSendMoney.setTextColor((tvSendMoney.getCurrentTextColor() == getColor(R.color.color_link)) ? getColor(R.color.color_gray_dark) : getColor(R.color.color_link));
        });

        tvSendMoney.setOnClickListener(view -> {
            tvAccounts.setTextColor((tvAccounts.getCurrentTextColor() == getColor(R.color.color_link)) ? getColor(R.color.color_gray_dark) : getColor(R.color.color_link));
            tvSendMoney.setTextColor((tvSendMoney.getCurrentTextColor() == getColor(R.color.color_link)) ? getColor(R.color.color_gray_dark) : getColor(R.color.color_link));
        });
    }

    @Override
    public void addCard() {
        tvAddCard.setOnClickListener(v -> {
            tvAddCard.setTextColor(getColor(R.color.color_primary));
            update = true;
            Intent intent = new Intent(this, FormActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void configRecyclerBalance() {
        rvBalance.setHasFixedSize(true);
        rvBalance.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        adapterBalance = new MainActivityAdapterBalance(this);
        rvBalance.setAdapter(adapterBalance);
    }

    @Override
    public void configRecyclerCards() {
        rvCard.setHasFixedSize(true);
        rvCard.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvCard.setContextClickable(false);
        rvCard.setNestedScrollingEnabled(false);
        rvCard.suppressLayout(true);
        adapterCard = new MainActivityAdapterCards(this);
        rvCard.setAdapter(adapterCard);
    }

    @Override
    public void configRecyclerTransaction() {
        rvTransaction.setHasFixedSize(true);
        rvTransaction.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvTransaction.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        adapterTransaction = new MainActivityAdapterTransaction(this);
        rvTransaction.setAdapter(adapterTransaction);
    }

    @Override
    public void showAccount(Cuenta cuenta) {
        String lastSession = cuenta.getUltimaSesion();
        lastSession = lastSession.substring(0, lastSession.length() - 8);
        tvAccountName.setText(cuenta.getNombre());
        tvAccountDate.setText(String.format("Ultimo inicio %s", lastSession));
    }

    @Override
    public void showBalances(Saldo saldo) {
        adapterBalance.addList(saldo);
    }

    @Override
    public void showCards(Tarjeta tarjeta) {
        adapterCard.addList(tarjeta);
    }

    @Override
    public void showTransactions(Movimiento movimiento) {
        adapterTransaction.addList(movimiento);
    }

    @Override
    public void showSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}