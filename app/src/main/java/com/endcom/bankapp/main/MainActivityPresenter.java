package com.endcom.bankapp.main;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class MainActivityPresenter implements MainActivityMVP.Presenter {

    private static final String TAG = MainActivityPresenter.class.getName();

    private final MainActivityMVP.Model model;
    private MainActivityMVP.View view;

    private Disposable subscription;

    public MainActivityPresenter(MainActivityMVP.Model model) {
        this.model = model;
    }

    @Override
    public void setView(MainActivityMVP.View view) {
        this.view = view;
    }

    @Override
    public void rxJavaUnsubscribe() {
        if (subscription != null && !subscription.isDisposed()) subscription.dispose();
    }

    @Override
    public void loadAccount() {
        if (view == null) return;
        model.resultAccount()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> subscription = disposable)
                .doOnNext(cuenta -> {
                    if (view != null) view.showAccount(cuenta);
                })
                .doOnError(throwable -> view.showError(throwable.getMessage()))
                .doOnComplete(() -> view.showSuccess("Carga completa.."))
                .subscribe();
    }

    @Override
    public void loadBalance() {
        if (view == null) return;
        model.resultBalance()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> subscription = disposable)
                .doOnNext(saldos -> {
                    if (view != null) view.showBalances(saldos);
                })
                .doOnError(throwable -> view.showError(throwable.getMessage()))
                .doOnComplete(() -> view.showSuccess("Carga completa.."))
                .subscribe();
    }

    @Override
    public void loadCard() {
        if (view == null) return;
        model.resultCard()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> subscription = disposable)
                .doOnNext(tarjeta -> {
                    if (view != null) view.showCards(tarjeta);
                })
                .doOnError(throwable -> view.showError(throwable.getMessage()))
                .doOnComplete(() -> view.showSuccess("Carga completa.."))
                .subscribe();
    }

    @Override
    public void loadTransaction() {
        if (view == null) return;
        model.resultTransaction()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> subscription = disposable)
                .doOnNext(movimiento -> {
                    if (view != null) view.showTransactions(movimiento);
                })
                .doOnError(throwable -> view.showError(throwable.getMessage()))
                .doOnComplete(() -> view.showSuccess("Carga completa.."))
                .subscribe();
    }
}
